<?php

namespace Drupal\ga_webform\Plugin\WebformHandler;

use Drupal\ga\AnalyticsCommand\Event;
use Drupal\webform\Plugin\WebformHandlerBase;
use Drupal\webform\WebformSubmissionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Registers a Googalytics tracking event.
 *
 * Note, that this webform handler must bridge the gap form the request to the
 * response, as the page gets either redirected or reloaded. It must register
 * the event in the system. Upon delivering the response, the corresponding
 * Googalytics event handler will take care of firing the tracking event.
 *
 * @WebformHandler(
 *   id = "googalytics_event",
 *   label = @Translation("Googalytics event"),
 *   description = @Translation("Registers a Googalytics tracking event."),
 *   cardinality = \Drupal\webform\Plugin\WebformHandlerInterface::CARDINALITY_SINGLE,
 *   results = \Drupal\webform\Plugin\WebformHandlerInterface::RESULTS_IGNORED,
 *   submission = \Drupal\webform\Plugin\WebformHandlerInterface::SUBMISSION_OPTIONAL,
 * )
 */
class GoogalyticsWebformHandler extends WebformHandlerBase {

  /**
   * The delayed command registry.
   *
   * @var \Drupal\ga_webform\DelayedCommandRegistryInterface
   */
  protected $delayedCommandRegistry;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->delayedCommandRegistry = $container->get('ga_webform.delayed_command_registry');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function postSave(WebformSubmissionInterface $webform_submission, $update = TRUE) {
    $command = new Event('webform', 'submit', $webform_submission->getWebform()->id());
    $this->delayedCommandRegistry->addCommand($command);
  }

  /**
   * {@inheritdoc}
   */
  public function getSummary() {
    // This handler does not need a summary.
    return [];
  }

}
